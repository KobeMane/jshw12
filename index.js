//  потому что их не достаточно может быть, у инпута есть свои события для отслеживания.

const btn = document.querySelectorAll(".btn");
const btnArr = [...btn];

document.body.addEventListener("keypress", function (key) {
  let btn;
  btnArr.forEach((button) => {
    btn = button;
    console.log(button);
    if (key.code.slice(3) == btn.innerText || key.code == btn.innerText) {
      button.style.background = "blue";
    } else if (key.code.slice(3) !== btn.innerText) {
      button.style.background = "black";
    }
  });
});
